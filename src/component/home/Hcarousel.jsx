import Slider from '@ant-design/react-slick';
import React from 'react'
import photos from '../../img/image-removebg-preview (1) 1.png'
import '../../style/home/hcarousel.scss'


export const Hcarousel = () => {
  var settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <div className='h-carousel'>
      <Slider {...settings}>
        <div className='h-carousel-item'>
          <div className='row'>
            <div className='col-lg-5 h-carousel-item-info'>
              <h6>Bizning eng mashur brendlarimiz </h6>
              <h1>Blouses & Tops</h1>
              <p>
                One morning, when Gregor Samsa woke from troubled dreams, he found himself
                transformed in his bed into a horrible vermin.
              </p>
            </div>
            <div className='col-lg-7'>
              <img src={photos} alt='' />
            </div>
          </div>
        </div>
        <div>
          <h3>2</h3>
        </div>
        <div>
          <h3>3</h3>
        </div>
      </Slider>
    </div>
  )
}
