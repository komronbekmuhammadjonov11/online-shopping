import React from 'react'
import '../../style/home/arrivalBanner.scss'

export const ArrivalBanner = () => {
  return (
    <div className='row arrival-banner-wrapper'>
      <div className='col-lg-12 arrival-banner'>
        <h4>New Arrivals</h4>
        <p>
          One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed i
          nto a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could
          see his brown belly, slightly domed and divided by arches into stiff sectionsF
        </p>
      </div>
    </div>
  )
}
