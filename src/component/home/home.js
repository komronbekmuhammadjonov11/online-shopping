import React from 'react';
import '../../style/home/home.scss'
import { ArrivalBanner } from './ArrivalBanner';
import { Delivery } from './Delivery';
import { DeliveryQuality } from './DeliveryQuality';
import { Hcarousel } from './Hcarousel';
import { Products } from './Products';
import { SaleBlock } from './SaleBlock';
import { Thougth } from './Thougth';

export const Home = () => {
    return (
        <div className="home">
            <Hcarousel />
            <ArrivalBanner />
            <Products />
            <SaleBlock />
            <Thougth />
            <Delivery />
            <DeliveryQuality />
        </div>
    )
};
