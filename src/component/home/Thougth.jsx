import React from 'react'
import '../../style/home/thought.scss'
import photos from '../../img/Rectangle.png'
import frame from '../../img/Frame.png'

export const Thougth = () => {
  return (
    <div className='row thought-wrapper'>
      <div className="col-lg-5 offset-1 thought-img">
        <img src={photos} alt='' />
      </div>
      <div className="col-lg-5 thought-info">
        <h4>
          “With BlockBox, we have
          seen 30% revenue growth quarter over quarter and
          now we’re rolling it out across the entire customer lifecycle.”
        </h4>
        <p>kevin Pietersen, <span>VP of Marketing</span></p>
        <img src={frame} alt='' />
      </div>
    </div>
  )
}
