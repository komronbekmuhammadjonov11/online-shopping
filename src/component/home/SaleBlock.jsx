import React from 'react'
import { Link } from 'react-router-dom'
import '../../style/home/saleBlock.scss'
import photos from  '../../img/sale.png'

export const SaleBlock = () => {
  return (
    <div className='row sale-block-wrapper'>
      <div className="col-lg-6 offset-1 sale-block-info ">
        <p className='deadline'>Faqat 1 hafta</p>
        <h3 className='new-sale'>Yangicha chegirma</h3>
        <div className='sale-cost'>
          <span className='line-through'>120 000so`m</span>
          <span>100 000so`m</span>
        </div>
        <span className='sale-percent'>
          20%
        </span>
        <div className='sale-finished'>
          <div className='sale-date-block'>
            <h6>14</h6>
            <p>days</p>
          </div>
          <div className='sale-date-block'>
            <h6>14</h6>
            <p>hours</p>
          </div>
          <div className='sale-date-block'>
            <h6>14</h6>
            <p>min</p>
          </div>
          <div className='sale-date-block'>
            <h6>14</h6>
            <p>seconds</p>
          </div>
        </div>
        <Link to='/' className='view-link'>Ko`rish</Link>
      </div>
      <div className="col-lg-5 sale-block-photos">
        <img src={photos} alt='' />
      </div>
    </div>
  )
}
