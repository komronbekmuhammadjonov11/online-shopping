import React from 'react'
import '../../style/home/deliveryQuaity.scss'
import advantage_1 from '../../img/advantage-1.png'
import advantage_2 from '../../img/advantage-2.png'
import advantage_3 from '../../img/advantage-3.png'
import advantage_4 from '../../img/advantage-4.png'

export const DeliveryQuality = () => {
  return (
    <div className='row delivery-quality-wrapper'>
      <div className="col-lg-12 dq-block">
        <div className='dq-item'>
          <img src={advantage_1} alt='' />
          <div className='dq-item-info'>
            <h4>Mahsulotni sotib olish</h4>
            <p>
              Bizning mahsulotlardan o’zingizga
              yoqganini tanlab olasiz va online
              to’lov qilasiz.
            </p>
          </div>
        </div>
        <div className='dq-item'>
          <img src={advantage_2} alt='' />
          <div className='dq-item-info'>
            <h4>Mahsulotni sotib olish</h4>
            <p>
              Bizning mahsulotlardan o’zingizga
              yoqganini tanlab olasiz va online
              to’lov qilasiz.
            </p>
          </div>
        </div>
        <div className='dq-item'>
          <img src={advantage_3} alt='' />
          <div className='dq-item-info'>
            <h4>Mahsulotni sotib olish</h4>
            <p>
              Bizning mahsulotlardan o’zingizga
              yoqganini tanlab olasiz va online
              to’lov qilasiz.
            </p>
          </div>
        </div>
        <div className='dq-item'>
          <img src={advantage_4} alt='' />
          <div className='dq-item-info'>
            <h4>Mahsulotni sotib olish</h4>
            <p>
              Bizning mahsulotlardan o’zingizga
              yoqganini tanlab olasiz va online
              to’lov qilasiz.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
