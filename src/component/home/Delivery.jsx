import React from 'react'
import '../../style/home/delivery.scss'

export const Delivery = () => {
  return (
    <div className='row delivery-wrapper'>
      <div className='col-lg-12 delivery'>
        <h4>Bizning mahsulotlar sizga shunday yetib boradi</h4>
        <p>
          Mahsulotlarimiz butun O’zbekistonda mashhur bo’lgan chet el mahsulotlari.
        </p>
      </div>
    </div>
  )
}
