import React from 'react'
import { Products } from '../home/Products'
import productsImg from '../../img/Rectangle 44.png'
import productsImg2 from '../../img/image 5.png'
import stars from '../../img/stars.png'
import '../../style/insideProducts/insideProducts.scss'
import { Link } from 'react-router-dom'

export const InsideProduct = () => {
  return (
    <div className='inside-product-wrapper'>
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb py-3">
                <li class="breadcrumb-item"><Link to="/">Home</Link></li>
                <li class="breadcrumb-item"><Link to="/products">Products</Link></li>
                <li class="breadcrumb-item active" aria-current="page">1</li>
              </ol>
            </nav>
          </div>
          <div className="col-lg-12">
            <div className="row">
              <div className="col-lg-6 product-img">
                <img src={productsImg} alt='' />
                <img src={productsImg2} alt='' />
                <span class=" translate-middle badge bg-danger">
                  20%
                </span>
              </div>
              <div className="col-lg-5 product-info-wrapper">
                <h4>White Tee</h4>
                <div className='product-cost'>
                  <div className='d-flex'>
                    <p className='line-through'>150 000so`m</p>
                    <p className='text-success'>120 000so`m</p>
                  </div>
                  <img src={stars} alt='' />
                </div>
                <p className='product-info'>
                  Samsa was a travelling salesman - and above
                  it there hung a picture that he had recently cut
                  out of an illustrated magazine and housed in a
                  nice, gilded frame.
                </p>
                <div class="mb-3">
                  <label for="email" class="form-label">O`lcham (required) </label>
                  <select class="form-select" aria-label="Default select example">
                    <option selected></option>
                    <option value="small">Small</option>
                    <option value="medium">Medium</option>
                    <option value="large">Large</option>
                  </select>
                </div>
                <div class="mb-3">
                  <label for="email" class="form-label">Rangi (color) </label>
                  <select class="form-select" aria-label="Default select example">
                    <option selected></option>
                    <option value="1">One</option>
                    <option value="2">Two</option>
                    <option value="3">Three</option>
                  </select>
                </div>
                <div className='count-wrapper'>
                  <span className='count-block'>
                    0
                  </span>
                  <button className='btn btn-dark'>Harid qilish</button>
                </div>
                <div className='add-list-wrapper'>
                  <svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10 19C9.35502 18.428 8.62602 17.833 7.85502 17.2H7.84502C5.13002 14.98 2.05302 12.468 0.694023 9.458C0.24754 8.49973 0.0109268 7.45713 1.1406e-05 6.4C-0.00297039 4.94949 0.578794 3.55899 1.61383 2.54277C2.64887 1.52655 4.04981 0.970397 5.50002 1C6.68065 1.00187 7.83586 1.34308 8.82802 1.983C9.26399 2.26597 9.65844 2.60826 10 3C10.3435 2.6098 10.7381 2.26771 11.173 1.983C12.1648 1.34296 13.3197 1.00172 14.5 1C15.9502 0.970397 17.3512 1.52655 18.3862 2.54277C19.4213 3.55899 20.003 4.94949 20 6.4C19.9898 7.45882 19.7532 8.5032 19.306 9.463C17.947 12.473 14.871 14.984 12.156 17.2L12.146 17.208C11.374 17.837 10.646 18.432 10.001 19.008L10 19ZM5.50002 3C4.56853 2.98835 3.67009 3.34485 3.00002 3.992C2.35441 4.62617 1.99358 5.49505 1.99994 6.4C2.01135 7.1705 2.18585 7.92985 2.51202 8.628C3.15353 9.92671 4.01913 11.1021 5.06902 12.1C6.06002 13.1 7.20002 14.068 8.18602 14.882C8.45902 15.107 8.73702 15.334 9.01502 15.561L9.19002 15.704C9.45702 15.922 9.73302 16.148 10 16.37L10.013 16.358L10.019 16.353H10.025L10.034 16.346H10.039H10.044L10.062 16.331L10.103 16.298L10.11 16.292L10.121 16.284H10.127L10.136 16.276L10.8 15.731L10.974 15.588C11.255 15.359 11.533 15.132 11.806 14.907C12.792 14.093 13.933 13.126 14.924 12.121C15.9741 11.1236 16.8397 9.94852 17.481 8.65C17.8131 7.9458 17.9901 7.17851 18.0001 6.4C18.0042 5.49784 17.6435 4.6323 17 4C16.3312 3.34992 15.4326 2.99049 14.5 3C13.3619 2.99033 12.274 3.46737 11.51 4.311L10 6.051L8.49002 4.311C7.72609 3.46737 6.6381 2.99033 5.50002 3Z" />
                  </svg>
                  <p>Ro`yhatga qo`shish</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Products />
    </div>
  )
}
