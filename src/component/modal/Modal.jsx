import React, { useState } from 'react'
import '../../style/modal/modal.scss'

export const Modal = () => {
  const [change, setChange] = useState(true)

  let classNames = require('classnames');

  let loginActive = classNames({
    'modal-title': true,
    active: change
  })
  let registerActive = classNames({
    'modal-title': true,
    active: !change
  })

  return (
    <>
      <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <p className={loginActive} onClick={() => setChange(true)}>Login</p>
              <p className={registerActive} onClick={() => setChange(false)}>Register</p>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              {
                change
                  ?
                  <>
                    <div class="mb-3">
                      <label for="email" class="form-label">Email *</label>
                      <input type="email" class="form-control" id="email" placeholder="Email..." />
                    </div>
                    <div class="mb-3">
                      <label for="parol" class="form-label">Password *</label>
                      <input type="password" class="form-control" id="parol" placeholder="Password..." />
                    </div>
                    <a href='#'>Parolni unutdingizmi?</a>
                  </>
                  :
                  <>
                    <div class="mb-3">
                      <label for="name" class="form-label">Name *</label>
                      <input type="text" class="form-control" id="name" placeholder="Name..." />
                    </div>
                    <div class="mb-3">
                      <label for="manzil" class="form-label">Address *</label>
                      <input type="text" class="form-control" id="manzil" placeholder="Address..." />
                    </div>
                    <div class="mb-3">
                      <label for="email" class="form-label">Email *</label>
                      <input type="email" class="form-control" id="email" placeholder="Email..." />
                    </div>
                    <div class="mb-3">
                      <label for="parol" class="form-label">Password *</label>
                      <input type="password" class="form-control" id="parol" placeholder="Password..." />
                    </div>
                  </>
              }
            </div>
            <div className="modal-footer">
              <button className='btn w-100 btn-outline-dark'>
                {
                  change
                    ? 'Login'
                    : 'Register'
                }
              </button>
              {/* <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button> */}
            </div>
          </div>
        </div>
      </div>
    </>
  )
};
