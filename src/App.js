import React from "react";
import './App.css';
import { Home } from "./component/home/home";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Navbar } from "./component/navbar/navbar";
import { Footer } from "./component/footer/Footer";
import { InsideProduct } from "./component/insideProduct/InsideProduct";

function App() {
  return (
    <div className="container-fluid project">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" exact element={<Home />} />
          <Route path="/products/:id" element={<InsideProduct />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
